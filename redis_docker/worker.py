import json
from hotqueue import HotQueue
import redis
import requests
from datetime import datetime
import job as job_lib

q=HotQueue("queue", host="redis", port=6379,  db=15)
job_archive=redis.StrictRedis(host='redis', port=6379, db=7)  #stores archive of jobs
output_db=redis.StrictRedis(host='redis', port=6379, db=8) 

def generate_job_key(jid):
	return 'job.{}'.format(jid)

def saveoutput(job, jid, jobkey, r):
	job['ended']=str(datetime.now())
	if "Error" in str(r) :
		job['status']='error'
		output={"message": "Job Error", "response": r }
		output_db.set(jid, json.dumps(output))
	else:
		job['status']='completed'
		r=json.loads(r)
		output={"message": "Job successfully completed", "response": r}
		output_db.set(jid, json.dumps(output))

	job_archive.set(jobkey, json.dumps(job))
	print(r)
	print('status', job['status'])

@q.worker
def do_work(jid):
	
	jobkey=generate_job_key(str(jid))
	print('------------------')
	print(jobkey)
	print(job_archive.get(jobkey))
	job=json.loads(job_archive.get(jobkey))
	job['started']=str(datetime.now())
	job['status']='in progress'
	job_archive.set(jobkey, json.dumps(job))

	if (job["command"]=="query_daterange"):
		parameters=job['parameters']
		print('query datarange')
		r=job_lib.querydates(parameters)
		saveoutput(job, jid, jobkey, r)	
		return 'task sent to api!'
	elif (job["command"]=="mean"):
                print('mean')
                r=job_lib.mean_score()
                saveoutput(job, jid, jobkey, str(r))
                return 'task sent to api!'
	elif ( job["command"] == "create_new"):
		parameters = job['parameters']
		print('create new data point')
		r = job_lib.new_item(parameters)
		job['ended'] = str(datetime.now())
		saveoutput(job, jid, jobkey, str(r))
		return 'adding new data point task sent to api'
	elif (job["command"] == "list_all"):
		print('listing the entier data set')
		r = job_lib.list_all()  
		job['ended'] = str(datetime.now())
		saveoutput(job,jid,jobkey,str(r))
		print('status',job['status'])
		return 'listing all data point task sent to api'
	elif (job["command"] == "plot"):
		print('generate plot')
		r = job_lib.plot(job["id"])
		saveoutput(job, jid, jobkey, str(r))
		return 'task sent to api'
	else:
		q.put(item)

print(len(q))
do_work()

