
from flask import Flask, request, jsonify, json
import redis, uuid
from datetime import datetime
from hotqueue import HotQueue
# import termplotlib as tpl
import numpy as np

app=Flask(__name__)

q=HotQueue("queue", host='redis', port=6379, db=15)
job_archive=redis.StrictRedis(host='redis', port=6379, db=7)
output_db=redis.StrictRedis(host='redis', port=6379, db=8)

def importData(filename):
    with open(filename) as json_file:
        data=json.load(json_file)
    return data;

def generate_jid():
	return str(uuid.uuid4())

def generate_job_key(jid):
	return 'job.{}'.format(jid)

#creates Python object representing a job
def instantiate_job(jid, command, parameters, timestamp):
	return {'id': jid,
		'status': "SUBMITTED",
		'command': command,
		'parameters': parameters,
		'time_created': timestamp,
		'started': "N/A",
		'ended': "N/A"}
	
def new_job(job, jid):

	##add item to archive database
	key=generate_job_key(jid)
	job_archive.set(key, job)

	##add item to queue
	q.put(jid)
	print(jid)
	return 'Job ID:' + jid


data=importData('FoodReviews.json');

@app.route('/', methods=['GET'])
def hello_world():
	return 'Welcome to our API!\n'

@app.route('/entrycount', methods=['GET'])
def entrycount():
	c=0
	for item in data:
		c=c+1

	return 'entry count: ' + str(c)+'\n'

@app.route('/query_daterange', methods=['GET'])
def querydates():
	start=request.args.get('start')
	end=request.args.get('end')
	
	command="query_daterange"
	parameters={'start': start, 'end': end}
	timestamp=str(datetime.now())
	jid= generate_jid()
	job=instantiate_job(jid, command, parameters, timestamp)
	new_job(json.dumps(job), jid)
	
	return 'job id: '+ jid + '\n'

@app.route('/mean_score', methods = ['GET'])
def mean_score():
	command="mean"
	parameters={}
	timestamp=str(datetime.now())
	jid= generate_jid()
	job=instantiate_job(jid, command, parameters, timestamp)
	new_job(json.dumps(job), jid)

	return 'job id: '+ jid + '\n'

#add new item(s) to the list
@app.route('/add',methods = ['POST'])
def create_item():
	#score = request.args.get('score')
	#restaurant_name = request.args.get('restaurant_name')
	#facility_id = request.args.get('facility_id')
	#inspection_date = request.args.get('inspection_date')
	#longitude = request.args.get('longitude')
	#latitude = request.args.get('latitude')
	data=request.get_json(force=True)
	parameters={'newdata': json.dumps(data)}
	timestamp=str(datetime.now())
	command = "create_new"
	
	jid = generate_jid()
	job = instantiate_job(jid, command, parameters, timestamp)
	new_job(json.dumps(job),jid)
	
	return 'job id: '+ jid + '\n'

@app.route('/list_all',methods = ['GET'])
def list_all():
	command = "list_all"
	parameters = {}
	timestamp = str(datetime.now())
	jid = generate_jid()
	job = instantiate_job(jid,command,parameters,timestamp)
	new_job(json.dumps(job),jid)
	
	return 'job id: ' + jid + '\n'


@app.route('/plot', methods = ['GET'])
def plot():
	command = "plot"
        parameters = {}
        timestamp = str(datetime.now())
        jid = generate_jid()
        job = instantiate_job(jid,command,parameters,timestamp)
        new_job(json.dumps(job), jid)

        return 'job id: ' + jid + '\n'
 
		

@app.route('/jobs/<job_id>', methods=['GET'])
def jobstatus(job_id):
	key= generate_job_key(job_id)
	j= json.loads(job_archive.get(key))

        if j["status"]=='completed' or j['status']=="error":
                o=json.loads(output_db.get(job_id))

		print(o)

                return  jsonify(o)  #'{} \n\n{}\n'.format(o, json.dumps(j))

	else:
		return jsonify(j)

@app.route('/jobs/info/<job_id>', methods=['GET'])
def jobdetails(job_id):
	key= generate_job_key(job_id)
	j= json.loads(job_archive.get(key))
	return jsonify(j)


if __name__ =='__main__':
	app.run(debug=True, host='0.0.0.0')









