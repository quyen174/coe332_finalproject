
from flask import Flask, request, jsonify, json
from datetime import datetime
import sys


def importData(filename):
    with open(filename) as json_file:
        data=json.load(json_file)
    return data;


data=importData('FoodReviews.json');


def hello_world():
	return 'you are in job API!\n'

def entrycount():
	c=0
	for item in data:
		c=c+1

	return json.dumps({'entry count': str(c)})

def querydates(parameters):
	try:
		
		start=parameters["start"]
		end=parameters["end"]
	
		start=datetime.strptime(start, '%m-%d-%Y').date()
		end=datetime.strptime(end, '%m-%d-%Y').date()

		data= importData('FoodReviews.json');
		dataq=[]

		for item in data:
			c_date=item["inspection_date"]
			c_date=datetime.strptime(c_date, "%Y-%m-%dT%H:%M:%S.%f").date()
			if start <= c_date and c_date <= end:
				dataq.append(item)

		return json.dumps(dataq)
	except Exception as e:
		print(e)
		return "Error: "+ str(e) 


def mean_score():
	total_score = 0
	for item in data:
		num_score = int(item['score'])
		total_score = total_score + num_score
		mean_score = total_score/1000
	return json.dumps({'mean': mean_score})

def plot(jid):
	import cloudinary
	import cloudinary.uploader as Upload
	cloudinary.config(cloud_name='dztzaspcf', api_key='335364319253852', api_secret='l9p1s3kU5aW1fJTV0nll2iS12J8')
	import pandas as pd
	import numpy as np
	import matplotlib
	matplotlib.use('agg')
	import matplotlib.pyplot as plt
	score=[]
	for item in data:
		score.append(int(item["score"]))
	print(len(score))
	plt.hist(score, bins=7)
	#df=pd.DataFrame(data=score)
	#df.plot.hist(bins=7)
	plt.xlabel('Score')
	plt.ylabel('Number of Restaurants')
	plt.title('Food Inspection Score Distribution')
	plt.savefig(jid + '.png') 
	response = Upload.upload(jid + '.png')
	url = response['url']
	print(url)
	# Returning the url pointing to the image.
	return json.dumps({"url": url})

def new_item(parameters):
	try:
		data = importData('FoodReviews.json');
		
		newdata=json.loads(parameters["newdata"])
		for item in newdata:
			#date = item["inspection_date"]
			#date = datetime.strptime(date, '%m-%d-%Y').date()

			new = {
				'process_description' : "Routine Inspection",
				'facility_id': item["facility_id"],
				'score' : item["score"],
				'restaurant_name': item ["restaurant_name"],
				'latitude': item ["latitude"],
				'longitude' : item ["longitude"],
				'inspection_date': item['inspection_date']
				}
			data.append(new)
		
		with open('FoodReviews.json','w') as outfile:
			json.dump(data, outfile)

		return json.dumps(newdata)

	except Exception as e:
		print(e)
		return "Error: "+ str(e)

def list_all():
	data = importData('FoodReviews.json');

	return json.dumps(data)

