# GET/entrycount

returns number of food report entries in FoodData.json

Example:
```python
GET /entrycount
"result": "entry count: 1000" 
```

# GET/jobs/<jobid> #

to pull the status of a summitted job using the returned job id.
if the job is completed or in error status, the route will return the output as well

Example: (missing end date)
```python
GET /query_daterange?start=03-03-2020
job id: 04a0c310-42c3-4ce9-8edc-3821cc8dc621

GET /jobs/04a0c310-42c3-4ce9-8edc-3821cc8dc6
{
  "message": "Job Error",
  "response": "Error: strptime() argument 1 must be string, not None"
}
```

# GET/jobs/info/<jobid> #

returns status information for the task with specified jobid

```python
GET /mean_score
job id: 7604bb31-71ec-4399-809a-6e9be16e9726

GET /jobs/info/7604bb31-71ec-4399-809a-6e9be16e9726
{
  "command": "mean",
  "ended": "2020-05-14 22:00:26.610469",
  "id": "7604bb31-71ec-4399-809a-6e9be16e9726",
  "parameters": {},
  "started": "2020-05-14 22:00:26.607286",
  "status": "completed",
  "time_created": "2020-05-14 22:00:26.595002"
}
```
# GET/list_all#
 
returns the list of entire data from FoodReviews.json

Example:
```python
GET /list_all
job id: 7d70dd5b-c249-4b46-a72b-cdee9a77fc44

GET /jobs/7d70dd5b-c249-4b46-a72b-cdee9a77fc44
{
  "message": "Job successfully completed",
  "response": [
    {
      ":@computed_region_8spj_utxs": "4",
      ":@computed_region_a3it_2a2z": "3641",
      ":@computed_region_e9j2_6w3z": "77",
      ":@computed_region_jcrc_4uuy": "40",
      ":@computed_region_m2th_e4b7": "217",
      ":@computed_region_q9nd_rr82": "9",
      ":@computed_region_rxpj_nzrk": "3",
      "address": {
        "human_address": "{\"address\": \"6929 AIRPORT BLVD\", \"city\": \"AUSTIN\", \"state\": \"TX\", \"zip\": \"78752\"}",
        "latitude": "30.336497",
        "longitude": "-97.718225"
      },
      "facility_id": "11785910",
      "inspection_date": "2018-10-24T00:00:00.000",
      "process_description": "Routine Inspection",
      "restaurant_name": "101 by Tea Haus LLC",
      "score": "87",
      "zip_code": "78752"
    },
    {
      ":@computed_region_8spj_utxs": "4",
      ":@computed_region_a3it_2a2z": "3641",
      ":@computed_region_e9j2_6w3z": "77",
      ":@computed_region_jcrc_4uuy": "40",
      ":@computed_region_m2th_e4b7": "217",
      ":@computed_region_q9nd_rr82": "9",
      ":@computed_region_rxpj_nzrk": "3",
      "address": {
        "human_address": "{\"address\": \"6929 AIRPORT BLVD\", \"city\": \"AUSTIN\", \"state\": \"TX\", \"zip\": \"78752\"}",
        "latitude": "30.336497",
        "longitude": "-97.718225"
      },
      "facility_id": "11785910",
      "inspection_date": "2020-01-07T00:00:00.000",
      "process_description": "Routine Inspection",
      "restaurant_name": "101 by Tea Haus LLC",
      "process_description": "Routine Inspection",
      "restaurant_name": "101 by Tea Haus LLC",
      "score": "94",
      "zip_code": "78752"
    },
    ...
```
Note: output has been truncated

# POST/add #

user inputs the data and it adds new item(s) into the FoodReviews.json
Note: data must be formatted as a json file composed of a list of dictionaries, where each dictionary entry corresponds to a particular review.

### Parameters ###
|Name|Details|Example
|---|-----|---|
|process_description|required, must be string| "regular check-up"|
|facility_id|required, must be string| "897865"|
|score|required, must be string|"90"|
|restaurant_name|required, must be string|"Taco Bell"|
|latitude|required, must be string|"97.124"|
|longitude|required, must be string|"30.12836"|
|inspection_date|required, must be string in format "YYYY-mm-ddTHH:MM:SS.fff"|"2020-03-05T00:00:00.000"


Example:
```python
POST /add -d '[{"process_description": "regular check-up","facility_id": "897865","score":"90" ,"restaurant_name":"Taco Bell" ,"latitude":"97.124" ,"longitude":"30.12836" ,"inspection_date":"2020-03-05T00:00:00.000"}]' ]
"job id: 1ac0d6bd-9094-49e5-892f-e304692aacc1"

POST /jobs/1ac0d6bd-9094-49e5-892f-e304692aacc1
{
  "message": "Job successfully completed",
  "response": [
    {
      "facility_id": "897865",
      "inspection_date": "2020-03-05T00:00:00.000",
      "latitude": "97.124",
      "longitude": "30.12836",
      "process_description": "regular check-up",
      "restaurant_name": "Taco Bell",
      "score": "90"
    }
  ]
}
```
	

# GET/mean_score#

returns the mean Food Inspection score in FoodReviews.json

Example:
```python
GET /mean_score
job id: a9fe9b64-ff92-4162-bc4c-d57b3983e09c

GET /jobs/a9fe9b64-ff92-4162-bc4c-d57b3983e09c
{
  "message": "Job successfully completed",
  "response": {
    "mean": 92
  }
}
```


# GET/query_daterange?start=<start_date>&end=<end_date>#

to extract food inspection reports between the start and end inspection date. dates must be entered in the following format: mm-dd-YYYY

Example:
```python
GET curl /query_daterange?start=03-03-2020&end=03-05-2020
job id: 4224f861-ad81-48ad-a886-2f4008604fb4

GET /jobs/4224f861-ad81-48ad-a886-2f4008604fb4
{
  "message": "Job successfully completed",
  "response": [
    {
      ":@computed_region_8spj_utxs": "7",
      ":@computed_region_a3it_2a2z": "3646",
      ":@computed_region_e9j2_6w3z": "45",
      ":@computed_region_jcrc_4uuy": "22",
      ":@computed_region_m2th_e4b7": "217",
      ":@computed_region_q9nd_rr82": "7",
      ":@computed_region_rxpj_nzrk": "18",
      "address": {
        "human_address": "{\"address\": \"9315 MC NEIL RD\", \"city\": \"AUSTIN\", \"state\": \"TX\", \"zip\": \"78758\"}",
        "latitude": "30.375651",
        "longitude": "-97.725479"
      },
      "facility_id": "11610713",
      "inspection_date": "2020-03-05T00:00:00.000",
      "process_description": "Routine Inspection",
      "restaurant_name": "Accent Food Service",
      "score": "100",
      "zip_code": "78758"
    },
    {
      ":@computed_region_8spj_utxs": "3",
      ":@computed_region_a3it_2a2z": "3256",
      ":@computed_region_e9j2_6w3z": "46",
      ":@computed_region_jcrc_4uuy": "8",
      ":@computed_region_m2th_e4b7": "114",
      ":@computed_region_q9nd_rr82": "3",
      ":@computed_region_rxpj_nzrk": "55",
      "address": {
        "human_address": "{\"address\": \"515 VARGAS RD\", \"city\": \"AUSTIN\", \"state\": \"TX\", \"zip\": \"78741\"}",
        "latitude": "30.237389",
        "longitude": "-97.691189"
      },
      "facility_id": "2800216",
      "inspection_date": "2020-03-05T00:00:00.000",
      "process_description": "Routine Inspection",
      "restaurant_name": "Allison Elementary",
      "score": "97",
      "zip_code": "78741"
    }
  ]
}
```

# GET/plot#

returns url link for histogram of food inspection scores

Example:
```python
GET /plot
job id: 9e074605-00c7-4a0c-9101-0e03574d4aef

GET /jobs/9e074605-00c7-4a0c-9101-0e03574d4aef
{
  "message": "Job successfully completed",
  "response": {
    "url": "http://res.cloudinary.com/dztzaspcf/image/upload/v1589498704/v2lawuvtfm0z2ofpzugw.png"
  }
}
```