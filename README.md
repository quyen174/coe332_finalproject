# README #

### Group Members ###

* Quyen Pham 
* Manisha Ganesh
* Erin Lee

### Description ###

* We created a dockerized API that allows a client to parse through metadata for Food Establisment Inspection Reports in the city of Austin
* The data we used was extracted from the following website: https://data.austintexas.gov/Health-and-Community-SRervices/Food-Establishment-Inspection-Scores/ecmv-9xxi
* For simplicity we used a sample set of the first 1000 entries, exported as a json file called FoodReviews.json
* Our flask server is hosted on the port 0.0.0.0:5051 and our redis databases are hosted on the port 0.0.0.0:6100
