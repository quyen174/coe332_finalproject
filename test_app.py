from app import app
import unittest, uuid



class BasicTests(unittest.TestCase):
	def setUp(self):
		# create a test client 
		self.app = app.test_client()
		# propagate the exception to the test client
		self.app.testing = True
		pass
	
	# executed after each test
	def tearDown(self):
		pass
	
	### tests ###
	def test_home_page(self):
		response = self.app.get('/')
		self.assertEqual(response.status_code,200)
		self.assertIn(b"Welcome to our API!\n", response.data)
	
	def test_entrycount(self):
		response = self.app.get('entrycount')
		self.assertEqual(response.status_code,200)
		self.assertIn(b"entry count: 1000\n",response.data)

	def test_mean_value(self):
		response = self.app.get('mean_score')
		self.assertEqual(response.status_code,200)
		self.assertIn(b"92",response.data)
	def test_plot(self):
		response=self.app.get('plot')
		self.assertEqual(response.status_code, 200)
		self.assertIn("url", response.data)
#	def test_query_daterange(self):	
#		response = self.app.get('query_daterange')
#		json_response = str(response.get_json())
#		self.assertEqual(response.status_code,200)
#		jid = uuid.uuid4()		
#		self.assertIn(str(jid),json_response)
		
if __name__ == "__main__":
	unittest.main()	
